import React, { useState, useEffect, useContext } from 'react';
import { Container, Row, Col, Form, Button } from "react-bootstrap";
import { Redirect } from 'react-router-dom';
import logo from '../images/logo02.jpg'
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Login(){

	const {user, setUser} = useContext(UserContext);
	const [email, setEmail] = useState('');
	const [password, setPassword] = useState('');	
	const [isActive, setIsActive] = useState(false);

	function authenticate(e) {

	    e.preventDefault();
         
            fetch('https://afternoon-stream-90692.herokuapp.com/users/login', {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    email: email,
                    password: password
                })
            })
            .then(res => res.json())
            .then(data => {
                console.log(data);

                if(typeof data.access !== "undefined"){
                    localStorage.setItem('token', data.access);
                    retrieveUserDetails(data.access);

                    Swal.fire({
                        title: "Login Successful",
                        icon: "success",
                        text: "01001000 01100001 01101001 01101100 00100000 01110100 01101000 01100101 00100000 01001111 01101101 01101110 01101001 01110011 01110011 01101001 01100001 01101000 00100001"
                    })
                }
                else{
                    Swal.fire({
                        title: "Authentication failed",
                        icon: "error",
                        text: "01010100 01101000 01100101 01110010 01100101 00100000 01101001 01110011 00100000 01101110 01101111 00100000 01110011 01110100 01110010 01100101 01101110 01100111 01110100 01101000 00100000 01101001 01101110 00100000 01100110 01101100 01100101 01110011 01101000 00101100 00100000 01101111 01101110 01101100 01111001 00100000 01110111 01100101 01100001 01101011 01101110 01100101 01110011 01110011 00101110"
                    })
                }
            })
	        setEmail('');
	        setPassword('');
	    }

	    const retrieveUserDetails = (token) => {
	                fetch('https://afternoon-stream-90692.herokuapp.com/users/details', {
	                    headers: {
	                        Authorization: `Bearer ${ token }`
	                    }
	                })
	                .then(res => res.json())
	                .then(data => {
	                    console.log(data);

	                    setUser({
	                        id: data._id,
	                        isAdmin: data.isAdmin
	                    })
	                })
	            }

	    		useEffect(() => {

	    	        if(email !== '' && password !== ''){
	    	            setIsActive(true);
	    	        }else{
	    	            setIsActive(false);
	    	        }

	    	    }, [email, password]);




	return(
		(user.id !== null) ?
                <Redirect to="/products" />
            :

		<Container>
			<Row>
				<Col className='mt-3 mb-3'>
					<img
						src={logo}
						class="rounded mx-auto d-block"
					 />
				</Col>
			</Row>

			<Row>
				<h1>
					Login
				</h1>
			</Row>

			<Row>
				<Col>
					<Form onSubmit={(e) => authenticate(e)} className="mt-3 mb-3">
					  <Form.Group className="mb-3" controlId="userEmail">
					    <Form.Label>Email Address</Form.Label>
					    <Form.Control 
                        	type="email" 
                       	 	placeholder="Enter Email" 
                        	value={email}
                        	onChange={(e) => setEmail(e.target.value)}
                        	required
                    	/>		     
					  </Form.Group>

					  <Form.Group className="mb-3" controlId="password">
					    <Form.Label>Password</Form.Label>
					    <Form.Control 
                        	type="password" 
                       	 	placeholder="Enter Password" 
                        	value={password}
                        	onChange={(e) => setPassword(e.target.value)}
                        	required
                    	/>
					  </Form.Group>
					  { isActive ? 
                    <Button variant="primary" type="submit" id="submitBtn">
                        Submit
                    </Button>
                    : 
                    <Button variant="danger" type="submit" id="submitBtn" disabled>
                        Submit
                    </Button>
                }

					</Form>
				</Col>
			</Row>
		</Container>
	)
}