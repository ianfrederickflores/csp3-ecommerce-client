import {Container} from 'react-bootstrap';
import {Row, Col, Card, Button} from "react-bootstrap";
import product01 from '../images/product01.jpg'

export default function Product01(){
	return(
		<Container>
			<Row>
				<Col className='mt-3 mb-3'>
					<img
						src={product01}
						class="rounded mx-auto d-block"
					 />
				</Col>
			</Row>

			<Row>
				<Col>
					<p>
						The Lasrifle is a directed-energy antipersonnel weapon used by the military forces of the Imperium of Man, and it is the most common and widely-used type of laser weapon in the galaxy. 
					</p>
					<p>
						The Lasgun uses a small portable capacitor power pack to produce a focused pinpoint laser beam which is strong enough to take an ordinary human arm off with one shot but is not as effective against the more durable alien bodies and stronger types of personal armour. 
					</p>
					<p>
						The Lasgun and Laspistol are weapons generally meant for dealing with lightly armoured infantry, and thus lack the brute power of more advanced weapons from the Imperium's arsenal 
					</p>
					<p>
						Despite its lack of damage output, the Lasgun remains a favoured weapon of Imperial soldiers, for it has many redeeming qualities. It is a solid and rugged weapon that remains reliable and precise in almost any environment, and requires very little maintenance -- a quick cleanup while mumbling a prayer to the weapon's Machine Spirit after usage is all that is needed to keep it going for over 10,000 shots, after which the focusing crystals must be replaced, an action that requires the intervention of a knowledgeable Tech-priest.
					</p>
				</Col>
			</Row>			

			<Row>
				<Col className='mt-3 mb-3'>
					<div class="card">
					  <div class="card-header">
					    Quote
					  </div>
					  <div class="card-body">
					    <blockquote class="blockquote mb-0">
					      <p>"The Demolisher, the Vanquisher, even the mighty Deathstrike Missile Launcher pale in comparison to the sheer firepower of trillions of lasguns unleashing hell in unison."</p>
					      <footer class="blockquote-footer">Captain Garius Septus of the 263rd Maccabian Janissaries</footer>
					    </blockquote>
					  </div>
					</div>
				</Col>
			</Row>					
		</Container>
	)
}