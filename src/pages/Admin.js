import React, { Fragment, useEffect, useState, useContext } from 'react';
import { Nav, Container, Card, Button, Row, Col, Image, CardGroup } from 'react-bootstrap';
import { Redirect } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import UserContext from '../UserContext';
import user from '../App';
import Table from "./CartView";
import TableRow from '../components/TableRow';
import setProducts from './Products';
import products from './Products';



export default function Admin(){

	const [allProducts, setAllProducts] = useState([]);

	useEffect(() => {
		fetch('https://afternoon-stream-90692.herokuapp.com/products')
		.then(res => res.json())
		.then(data => {

			setAllProducts(data.map(product => {
					return(
						<ProductCard key={product.id} productProp={product} />
					)
				})
			);
			
		})
	}, [])

	return(
		<Row className="mt-5">
		<Col xs={0} md={2} className="border-1">
		
			<Nav justify variant="pills" defaultActiveKey="/admin" className="flex-column bg-light text-light">
			  <Nav.Item>
			    <Nav.Link to={`/admin`}>Products</Nav.Link>
			  </Nav.Item>
			  <Nav.Item>
			    <Nav.Link to={`/admin/users`} eventKey="link-1">Users</Nav.Link>
			  </Nav.Item>
			  <Nav.Item>
			    <Nav.Link to={`/admin/orders`} eventKey="link-2">Orders</Nav.Link>
			  </Nav.Item>
			</Nav>

		</Col>

		<Col xs={12} md={10}>
			<Card>{allProducts}</Card>
		</Col>
		</Row>

	)
}