import React, { useState } from "react";
import Table from "./CartView";

const OrderHistory = ({ orders }) => {
  return <Table data={orders} />;
};

export default OrderHistory;
