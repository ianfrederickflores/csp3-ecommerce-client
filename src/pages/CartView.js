import React, { Fragment } from 'react';
import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, CardGroup } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import Checkout from './Checkout';
import CartCard from '../components/CartCard';
import TableRow from '../components/TableRow';
import ProductCard from '../components/ProductCard';

const myCart = [];

export default function CartView() {
	
	const [cart, setCart] = useState([]);
	const [cartItems, setCartItems] = useState([]);

	const userId = localStorage.getItem('id');

	useEffect(() => {
		fetch(`https://afternoon-stream-90692.herokuapp.com/cart/${userId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);			
			
			setCartItems(data[0].items.map(product => {

				myCart.push(product);

				const {_id, productId, name, description, quantity, price, subtotal} = product;
				
				return(<ProductCard key={product._id} props={product} />)
				}
			))
		})
	}, [])
	
	return(	
		<Table data={cartItems} />	
	)
}

const Table = function (props) {
  
	const userId = localStorage.getItem('id');
	var total = 0;
 
  return (

    <table className="table" striped bordered hover>
      <thead>
        <tr>
          <th>Name</th>
          <th>Description</th>
          <th>Price</th>
          <th>Quantity</th>
          <th></th>
          <th></th>
          <th>Subtotal</th>
          <th></th>
        </tr>
      </thead>

      <tbody>

        {myCart.map(function (props, index) {
        	
        	console.log(myCart);

          const itemId = props._id;
          const name = props.name;
          const description = props.description; 
          const price = props.price; 
          const quantity = props.quantity; 
          const subtotal = quantity * price;
          total += subtotal;

          return (
          	<Fragment>
            <tr id="itemId">
              <td>{name}</td>
              <td>{description}</td>
              <td>TG {price}</td>
              <td>{quantity}</td>
              <td><Link className="btn btn-warning">-</Link></td>
              <td><Link className="btn btn-success">+</Link></td>
              <td>TG {subtotal}</td>
              <td><Link className="btn btn-danger">Delete</Link></td>
            </tr>

            </Fragment>
          );
        })}
      </tbody>
      
      <thead>
        <tr>
          <th>Total</th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th></th>
          <th>TG {total}</th>
          <th></th>
        </tr>
      </thead>
    </table>
  );
};


