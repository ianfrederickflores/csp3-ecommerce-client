import {Container} from 'react-bootstrap';
import {Row, Col, Card, Button} from "react-bootstrap";
import product03 from '../images/product03.jpg'

export default function Product01(){
	return(
		<Container>
			<Row>
				<Col className='mt-3 mb-3'>
					<img
						src={product03}
						class="rounded mx-auto d-block"
					 />
				</Col>
			</Row>

			<Row>
				<Col>
					<p>
						Flamers, also known as "Flame Guns", are Flamer Weapons that come in a wide variety of designs and patterns, but all are ideal for flushing out enemies in cover and putting groups of foes to the torch with projected flame.
					</p>
					<p>
						The two most common variants of Flamers either have a detachable fuel canister under the barrel, or a hose connecting to a backpack canister. 
					</p>
					<p>
						Flamers are most commonly used by Imperial assault forces such as Space Marine Assault Squads, though xenos races such as the Eldar, Orks and the Tau are also known to make use of similar weapons. 
					</p>					
				</Col>
			</Row>

			<Row>
				<Col className='mt-3 mb-3'>
					<div class="card">
					  <div class="card-header">
					    Quote
					  </div>
					  <div class="card-body">
					    <blockquote class="blockquote mb-0">
					      <p>"By bolter shell, flamer burst and melta blast, the mutant, the heretic and the traitor alike are cleansed of their sin of existence. So has it been for five millennia, so shall it be unto the end of time."</p>
					      <footer class="blockquote-footer">Sister Immaculata, Words of Devotion, Verses IV-V, Chapter X, Volume LII</footer>
					    </blockquote>
					  </div>
					</div>
				</Col>
			</Row>						
		</Container>
	)
}