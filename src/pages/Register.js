import { useState, useEffect, useContext } from 'react';
import { Container } from 'react-bootstrap';
import { Row, Col, Form, Button } from "react-bootstrap";
import { Redirect, useHistory } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import logo from '../images/logo02.jpg'

export default function Register(){

	const {user} = useContext(UserContext);
    const history = useHistory();

    // State hooks to store the values of the input fields
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [mobileNum, setMobileNum] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState('');

    // Check if values are successfully binded
    console.log(email);
    console.log(password1);
    console.log(password2);

    function registerUser(e) {

        // Prevents page redirection via form submission
        e.preventDefault();

        fetch('https://afternoon-stream-90692.herokuapp.com/users/checkEmail', {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                email: email
            })
        })
        .then(res => res.json())
        .then(data => {

            console.log(data);

            if(data === true){

                Swal.fire({
                    title: 'Duplicate email found',
                    icon: 'error',
                    text: 'Please provide a different email.'   
                });

            } else {

                fetch('https://afternoon-stream-90692.herokuapp.com/users/register', {
                    method: "POST",
                    headers: {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify({
                        firstName: firstName,
                        lastName: lastName,
                        email: email,
                        username: username,
                        mobileNum: mobileNum,
                        password: password1
                    })
                })
                .then(res => res.json())
                .then(data => {

                    console.log(data);

                    if(data === true){

                        // Clear input fields
                        setFirstName('');
                        setLastName('');
                        setEmail('');
                        setUsername('');
                        setMobileNum('');
                        setPassword1('');
                        setPassword2('');

                        Swal.fire({
                            title: 'Registration Successful',
                            icon: 'success',
                            text: '01001000 01100001 01101001 01101100 00100000 01110100 01101000 01100101 00100000 01001111 01101101 01101110 01101001 01110011 01110011 01101001 01100001 01101000 00100001'
                        });

                        // Allows us to redirect the user to the login page after registering for an account
                        history.push("/login");

                    } else {

                        Swal.fire({
                            title: 'Something Wrong',
                            icon: 'error',
                            text: '01010100 01101000 01100101 01110010 01100101 00100000 01101001 01110011 00100000 01101110 01101111 00100000 01110011 01110100 01110010 01100101 01101110 01100111 01110100 01101000 00100000 01101001 01101110 00100000 01100110 01101100 01100101 01110011 01101000 00101100 00100000 01101111 01101110 01101100 01111001 00100000 01110111 01100101 01100001 01101011 01101110 01100101 01110011 01110011 00101110'   
                        });

                    };

                })
            };

        })

    }

useEffect(() => {

        // Validation to enable submit button when all fields are populated and both passwords match
        if((firstName !== '' && lastName !== '' && email !== '' && username !== '' && mobileNum.length === 11 && password1 !== '' && password2 !== '') && (password1 === password2)){
            setIsActive(true);
        } else {
            setIsActive(false);
        }

    }, [firstName, lastName, email, username, mobileNum, password1, password2]);





	return(
		<Container>
			<Row>
				<Col className='mt-3'>
					<img
						src={logo}
						class="rounded mx-auto d-block"
					 />
				</Col>
			</Row>

			<Row>
				<Col>
					<h1>
						Register
					</h1>
				</Col>
			</Row>

			<Row>
				<Col>					
					<Form onSubmit={(e) => registerUser(e)} className='mt-3 mb-3'>
					  <Row className="mb-3">
						<Form.Group as={Col} controlId="username">
					      <Form.Label>Username</Form.Label>
					      <Form.Control 
					    	    type="text" 
					    	    placeholder="Enter username"
					    	    value={username} 
					    	    onChange={e => setUsername(e.target.value)}
					    	    required
					    	/>
					    </Form.Group>

					    <Form.Group as={Col} controlId="email">
					      <Form.Label>Email</Form.Label>
					      <Form.Control 
					    	    type="email" 
					    	    placeholder="Enter Email"
					    	    value={email} 
					    	    onChange={e => setEmail(e.target.value)}
					    	    required
					    	/>
					    </Form.Group>

					    <Form.Group as={Col} controlId="password1">
					      <Form.Label>Password</Form.Label>
					      <Form.Control 
					    	    type="password" 
					    	    placeholder="Enter Password"
					    	    value={password1} 
					    	    onChange={e => setPassword1(e.target.value)}
					    	    required
					    	/>
					    </Form.Group>

					    <Form.Group as={Col} controlId="password2">
					      <Form.Label>Verify Password</Form.Label>
					      <Form.Control 
					    	    type="password" 
					    	    placeholder="Verify Password"
					    	    value={password2} 
					    	    onChange={e => setPassword2(e.target.value)}
					    	    required
					    	/>
					    </Form.Group>		    
					  </Row>

					  <Row className='mb-3'>
					  	<Form.Group as={Col} className="mb-3" controlId="firstName">
					    	<Form.Label>First Name</Form.Label>
					    	<Form.Control 
					    	    type="text" 
					    	    placeholder="Enter First Name"
					    	    value={firstName} 
					    	    onChange={e => setFirstName(e.target.value)}
					    	    required
					    	/>
					  	</Form.Group>

					  	<Form.Group as={Col} className="mb-3" controlId="lastName">
					    	<Form.Label>Last Name</Form.Label>
					    	<Form.Control 
					    	    type="text" 
					    	    placeholder="Enter Last Name"
					    	    value={lastName} 
					    	    onChange={e => setLastName(e.target.value)}
					    	    required
					    	/>
					  	</Form.Group>
					  </Row>

					  <Form.Group className="mb-3" controlId="mobileNumber">
					    <Form.Label>Mobile Number</Form.Label>
					    <Form.Control 
					    	    type="text" 
					    	    placeholder="Enter Mobile Number"
					    	    value={mobileNum} 
					    	    onChange={e => setMobileNum(e.target.value)}
					    	    required
					    	/>
					  </Form.Group>		  

					  { isActive ? 
					      <Button variant="primary" type="submit" id="submitBtn">
					          Submit
					      </Button>
					      : 
					      <Button variant="danger" type="submit" id="submitBtn" disabled>
					          Submit
					      </Button>
					  }

					</Form>
				</Col>
			</Row>	
		</Container>		
	)
}