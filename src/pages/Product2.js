import {Container} from 'react-bootstrap';
import {Row, Col, Card, Button} from "react-bootstrap";
import product02 from '../images/product02.jpg'

export default function Product01(){
	return(
		<Container>
			<Row>
				<Col className='mt-3 mb-3'>
					<img
						src={product02}
						class="rounded mx-auto d-block"
					 />
				</Col>
			</Row>

			<Row>
				<Col>
					<p>
						The Bolter is a large .75 calibre assault weapon. It has a much greater mass than most standard-issue Imperial weapons such as the Lasgun, although it is slightly shorter in length. Unlike most Imperial ranged infantry weapons, it lacks a stock, resulting in a grip much like a pistol's or a submachine gun's. 
					</p>
					<p>
						The most common standard patterns of Bolter can easily be wielded by a Space Marine, but on account of their size and weight, smaller human-scale pattern Bolters are used by the Battle-Sisters of the Adepta Sororitas. 
					</p>
					<p>
						The major advantage of the Bolter in comparison to the ubiquitous Lasgun is its stopping power, as the bolt's kinetic force and explosive head can deliver much more damage on a target than the coherent light beam from a Lasgun. 
					</p>					
				</Col>
			</Row>

			<Row>
				<Col className='mt-3 mb-3'>
					<div class="card">
					  <div class="card-header">
					    Quote
					  </div>
					  <div class="card-body">
					    <blockquote class="blockquote mb-0">
					      <p>"To a Space Marine, the boltgun is far more than a weapon; it is an instrument of Mankind's divinity, the bringer of death to his foes. Its howling blast is a prayer to the gods of battle."</p>
					      <footer class="blockquote-footer">From the teachings of Roboute Guilliman as laid down in the Apocrypha of Skaros</footer>
					    </blockquote>
					  </div>
					</div>
				</Col>
			</Row>						
		</Container>
	)
}