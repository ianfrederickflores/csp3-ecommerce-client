import React, { useState, useEffect } from 'react';
import { Container, Card, Button, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import ProductCard from '../components/ProductCard';
import CartView from './CartView';



export default function ProductView(props) {

	const productId = props.match.params.productId;
	const userId = localStorage.getItem('id');

	const [name, setName] = useState([]);
	const [description, setDescription] = useState([]);
	const [price, setPrice] = useState([]);

	useEffect(() => {
		fetch(`https://afternoon-stream-90692.herokuapp.com/products/${productId}`)
		.then(res => res.json())
		.then(data => {
			console.log(data);
				setName(data.name);
				setDescription(data.description);
				setPrice(data.price);
				}
			);
	}, [])


	const addToCart = async () => {
        fetch(`https://afternoon-stream-90692.herokuapp.com/cart/${userId}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                customerId: userId,
				productId: productId,
				name: name,
				description: description,
				price: price,
            })
        })
        .then(res => res.json())
        .then(data => {
            console.log(data);
   	    })
	}

	
	return(
		<Container className="mt-5">
				<Card>
					<Card.Body className="text-center">
						<Card.Title>{name}</Card.Title>
						<Card.Subtitle>Description</Card.Subtitle>
						<Card.Text>{description}</Card.Text>
						<Card.Subtitle>Price</Card.Subtitle>
						<Card.Text> TG {price}</Card.Text>						
						<Link className="btn btn-primary" onClick={addToCart} to={`/cart/${userId}`}>Add to Cart</Link>
					</Card.Body>
				</Card>
		</Container>
	)
}
