import { Container } from 'react-bootstrap';
import { Row, Col, Card, Button } from "react-bootstrap";
import { Link } from 'react-router-dom';
import logo from '../images/Home.jpg';
import product01 from '../images/product01.jpg';
import product02 from '../images/product02.jpg';
import product03 from '../images/product03.jpg';

export default function Home(){
	return(
		<Container>
			<Row>
				<Col className='mt-3 mb-3'>
					<img fluid
						src={logo}
						class="rounded mx-auto d-block"
						width="75%"						
					 />
				</Col>
			</Row>
			
			<Row>
				<h1 className='text-center'>
					Featured Products
				</h1>
			</Row>

			<Row className='mt-3 mb-3'>
				<Col>
					<Card style={{ width: '18rem' }}>
					  <Card.Img variant="top" src={product01} />
					  <Card.Body>
					    <Card.Title>Lasrifle</Card.Title>
					    <Card.Text>
					      The Lasgun is a directed-energy anti-personnel weapon used by the military forces of the Imperium of Man, and it is the most common and widely-used type of laser weapon in the galaxy.
					    </Card.Text>
					    <Link className="btn btn-primary" to={`/product1`}>
					    	Details
					    </Link>
					  </Card.Body>
					</Card>
				</Col>
				<Col>
					<Card style={{ width: '18rem' }}>
					  <Card.Img variant="top" src={product02} />
					  <Card.Body>
					    <Card.Title>Bolter</Card.Title>
					    <Card.Text>
					      The Bolter and its variants are some of the most powerful, hand-held, ballistic anti-personnel weaponry in use by the military forces of the Imperium of Man.
					    </Card.Text>
					    <Link className="btn btn-primary" to={`/product2`}>
					    	Details
					    </Link>
					  </Card.Body>
					</Card>
				</Col>
				<Col>
					<Card style={{ width: '18rem' }}>
					  <Card.Img variant="top" src={product03} />
					  <Card.Body>
					    <Card.Title>Flamer</Card.Title>
					    <Card.Text>
					      Flamer Weapons that come in a wide variety of designs and patterns, but all are ideal for flushing out enemies in cover and putting groups of foes to the torch with projected flame.
					    </Card.Text>
					    <Link className="btn btn-primary" to={`/product3`}>
					    	Details
					    </Link>
					  </Card.Body>
					</Card>
				</Col>				
			</Row>
		</Container>
	)
}