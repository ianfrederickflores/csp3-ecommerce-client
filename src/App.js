import { useState, useEffect } from 'react';
import { UserProvider } from './UserContext';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Route, Switch } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';
import Home from './pages/Home';
import Register from './pages/Register';
import Login from './pages/Login';
import Logout from './pages/Logout';
import Products from './pages/Products';
import Product1 from './pages/Product1';
import Product2 from './pages/Product2';
import Product3 from './pages/Product3';
import ProductView from './pages/ProductView';
import CartView from './pages/CartView';
import Checkout from './pages/Checkout';
import OrderHistory from './pages/OrderHistory';
import Admin from './pages/Admin';

import './App.css';


function App() {
  const [user, setUser] = useState({
      id: null,
      isAdmin: null
    })

  const unsetUser = () => {
      localStorage.clear();
    }

  useEffect(() => {
      console.log(user);
      console.log(localStorage);
    }, [user])


  return (
    <UserProvider value={{user, setUser, unsetUser}}>
      <Router>
        <AppNavbar />
        <Container>
          <Switch>
            <Route exact path="/" component={Home} /> 
            <Route exact path="/register" component={Register} />
            <Route exact path="/login" component={Login} />
            <Route exact path="/logout" component={Logout} />           
            <Route exact path="/products" component={Products} />
            <Route exact path="/product1" component={Product1} />
            <Route exact path="/product2" component={Product2} />
            <Route exact path="/product3" component={Product3} />
            <Route exact path="/products/:productId" component={ProductView} />
            <Route exact path="/cart/:userId" component={CartView} />
            <Route exact path="/orders/:userId" component={Checkout} />                             
            <Route exact path="/history" component={OrderHistory} />
            <Route exact path="/admin" component={Admin} />                                   
          </Switch>
        </Container>
      </Router>
    </UserProvider> 
  );
}

export default App;
