import React from "react";

function TableRow(cartProp) {
  return (
    <tr>
      <td>{cartProp.name}</td>
      <td>{cartProp.price}</td>
      <td>{cartProp.quantity}</td>
      <td>{cartProp.subtotal}</td>
      <td></td>
    </tr>
  );
}

export default TableRow;
