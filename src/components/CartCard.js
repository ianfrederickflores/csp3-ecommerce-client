import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Row, Col, Card } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import TableRow from '../components/TableRow';


export default function CartCard({cartProp}) {
	
	const userId = localStorage.getItem('id');
	const {_id, productId, name, description, quantity, price, subtotal} = cartProp;


	return (
		<TableRow data={cartProp} />
	)
}


CartCard.propTypes = {
	cart: PropTypes.shape({
		name: PropTypes.string.isRequired,
		price: PropTypes.number.isRequired,
		quantity: PropTypes.number.isRequired,
	}) 
}
