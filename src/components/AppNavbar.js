import { Fragment, useContext } from 'react';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import logo from '../images/logo02.jpg'
import { NavLink } from 'react-router-dom';
import { Form, Button } from "react-bootstrap";
import UserContext from '../UserContext';


export default function AppNavbar(){
    const { user } = useContext(UserContext);

	return(
		<Navbar class="p-3 mb-2 bg-light text-white" bg="dark" variant="dark" expand="lg">
            <Navbar.Brand as={NavLink} to="/" exact>
                <img className="logo"
                    src={logo}
                    width="100px"
                    height="50px"
                 />
                 Adeptus Mechanicus
            </Navbar.Brand>

            <Navbar.Toggle aria-controls="basic-navbar-nav" />

            <Navbar.Collapse id="basic-navbar-nav">
                <Nav className="ml-auto">                   
                    <Nav.Link as={NavLink} to="/products" exact>Products</Nav.Link>
                    {(user.id !== null) ?
                    <Fragment>
                        <Nav.Link as={NavLink} to="/logout" exact>Logout</Nav.Link>
                        <Nav.Link as={NavLink} to="/checkout" exact>Checkout</Nav.Link>
                        <Nav.Link as={NavLink} to="/history" exact>Order History</Nav.Link>
                    </Fragment>                    
                     :
                    <Fragment>
                        <Nav.Link as={NavLink} to="/register" exact>Register</Nav.Link>
                        <Nav.Link as={NavLink} to="/login" exact>Login</Nav.Link>
                    </Fragment>
                    }                   
                </Nav>
            </Navbar.Collapse>
        </Navbar>
	)
}